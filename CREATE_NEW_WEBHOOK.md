# Steps to create a new webhook

- Put the code for your new webhook in the file `webhook/newhook.py`, and the associated tests
  in `tests/test_newhook.py`.

- Create a new document `README.newhook.md` that describes the specific details for this
  webhook. Link to this file from the main [README.md](README.md) file.

- Create a new container image definition in the `builds/newhook.in` file. See the
  [Building container images](https://gitlab.com/cki-project/containers#building-container-images)
  section on the [CKI containers repository](https://gitlab.com/cki-project/containers) for
  documentation about how to build the container on your system. The `IMAGE_NAME` parameter
  is the name of the new webhook.

- Add a `publish_job` to the [.gitlab-ci.yml](.gitlab-ci.yml) file for the new webhook. The
  container image will be published as
  `registry.gitlab.com/cki-project/kernel-webhooks/newhook:latest`. Once your branch is pushed
  with these changes, a container image will be created that can be seen in the pipeline view.

# Steps to deploy the webhook into OpenShift

- Clone the [deployment-all](https://gitlab.cee.redhat.com/cki-project/deployment-all) repository.
  You will likely need to ask someone on the CKI team for access to this repository.

- Edit
  [secrets.yml](https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/secrets.yml)
  and search for the various `KERNEL_WEBHOOKS_` options and clone appropriately.

- Copy one of the
  [kernel-webhooks-](https://gitlab.cee.redhat.com/cki-project/deployment-all/-/tree/main/openshift)
  directories under the `openshift` directory into a new directory for your webhook.

- If the webhook requires persistent storage, then an example of how to do that can be found in the
  datawarehouse project:
  [30-storage-db.yml.in](https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/openshift/datawarehouse/30-storage-db.yml.in)
  and
  [50-deploymentconfig-db.yml.in](https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/openshift/datawarehouse/50-deploymentconfig-db.yml.in).
  Search for `db-data`.

  - If your persistent storage needs to be periodically updated via a cron job, then see the
    [kernel-webhooks schedules](https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/schedules/kernel-webhooks.yml)
    in the deployment-all repository. The
    [CKI scheduler documentation](https://gitlab.com/cki-project/cki-tools/-/blob/main/README.scheduler.md)
    is also another useful reference.

- Add the routing key to the `webhook_endpoints` section at the bottom of the file
  [gitlab-runner-config/runners.yaml](https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/gitlab-runner-config/runners.yaml)
  to configure the webhook to AMQP bridge with the necessary events.

- Edit deployment-all's
  [.gitlab-ci.yml](https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/.gitlab-ci.yml)
  file and add the `openshift_deploy` lines for the new webhook. Search for the
  `kernel-webhooks` comment and clone one of the existing webhooks.


# Readonly root inside container

Note that OpenShift mounts the container image as a readonly filesystem. If you need persistent
storage, then see above. If you need to write temporary files, then a writable location can be
setup in /data with the following snippet in the
[deployment-all](https://gitlab.cee.redhat.com/cki-project/deployment-all) repository. This will
most likely go in your `50-deploymentconfig-XXX.yml.in` file.

```
kind: DeploymentConfig
spec:
  template:
    spec:
      containers:
        - ...
          volumeMounts:
            - name: data
              mountPath: /data
      volumes:
        - name: data
          emptyDir: {}
```

# Need help?

If you encountered a bug with the kernel-webhooks, or want a new feature, then open an issue
on the [kernel-webhooks issue tracker](https://gitlab.com/cki-project/kernel-webhooks/-/issues).
Direct any questions to the [mailto:rh-kwf@redhat.com](rh-kwf@redhat.com) mailinglist.
