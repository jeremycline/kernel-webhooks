"""Fake classes."""
from unittest import mock

import gitlab


class FakeGitLabDict(dict):
    """Mimic GitLab access with a dictionary."""

    def get(self, key):
        """Override None with GitlabGetError if key doesn't exist."""
        try:
            return self[key]
        except KeyError:
            raise gitlab.GitlabGetError


class FakeGitLabMember:
    """Class representing member of group or project."""

    def __init__(self, username):
        """Initialize."""
        self.attributes = {'username': username}


class FakeGitLabMembers:
    """Manager class for group or project members."""

    def __init__(self):
        """Initialize."""
        self._member_list = []

    def all(self, **kwargs):
        """Get all members."""
        return self._member_list

    def add_member(self, username):
        """Add a member."""
        self._member_list.append(FakeGitLabMember(username))


class FakeMergeRequest:
    """Fake merge request."""

    def __init__(self, mr_id, attributes):
        """Initialize."""
        self.attributes = dict(iid=mr_id, **(attributes or {}))


class FakeGitLabProject:
    """GitLab project."""

    def __init__(self):
        """Initialize."""
        self.attributes = {}
        self.manager = mock.MagicMock()
        self.members = FakeGitLabMembers()
        self.mergerequests = FakeGitLabDict()

    def add_mr(self, mr_id, attributes=None):
        """Add a MR."""
        self.mergerequests[mr_id] = FakeMergeRequest(mr_id, attributes)


class FakeGitLabGroup:
    """GitLab group."""

    def __init__(self):
        """Initialize."""
        self.attributes = {}
        self.manager = mock.MagicMock()
        self.members = FakeGitLabMembers()


class FakeGitLab:
    """GitLab."""

    def __init__(self):
        """Initialize."""
        self.projects = FakeGitLabDict()
        self.groups = FakeGitLabDict()

    def add_project(self, project_name):
        """Add a project."""
        self.projects[project_name] = FakeGitLabProject()

    def add_group(self, group_name):
        """Add a group."""
        self.groups[group_name] = FakeGitLabGroup()

    def auth(self):
        """Mimic auth command."""
        pass
