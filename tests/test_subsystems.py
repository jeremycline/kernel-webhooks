"""Webhook interaction tests."""
import unittest
from unittest import mock

import webhook.common
import webhook.subsystems


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestSubsystems(unittest.TestCase):

    MAPPINGS = {'subsystems':
                [{'subsystem': 'MEMORY MANAGEMENT',
                  'labels': {'name': 'mm'},
                  'paths': {'includes': ['include/linux/mm.h', 'include/linux/vmalloc.h', 'mm/']}
                  },
                 {'subsystem': 'NETWORKING',
                  'labels': {'name': 'net',
                             'readyForMergeDeps': ['lnst']},
                  'paths': {'includes': ['include/linux/net.h', 'include/net/', 'net/']},
                  },
                 {'subsystem': 'XDP',
                  'labels': {'name': 'xdp'},
                  'paths': {'includes': ['kernel/bpf/devmap.c', 'include/net/page_pool.h'],
                            'includeRegexes': ['xdp']}
                  }]
                }

    FILE_CONTENTS = (
        '---\n'
        'subsystems:\n'
        ' - subsystem: MEMORY MANAGEMENT\n'
        '   labels:\n'
        '     name: mm\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/mm.h\n'
        '          - include/linux/vmalloc.h\n'
        '          - mm/\n'
        ' - subsystem: NETWORKING\n'
        '   labels:\n'
        '     name: net\n'
        '     readyForMergeDeps:\n'
        '       - lnst\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/net.h\n'
        '          - include/net/\n'
        '          - net/\n'
        ' - subsystem: XDP\n'
        '   labels:\n'
        '     name: xdp\n'
        '   paths:\n'
        '       includes:\n'
        '          - kernel/bpf/devmap.c\n'
        '          - include/net/page_pool.h\n'
        '       includeRegexes:\n'
        '          - xdp\n'
        )

    CHANGES = {'changes': [{'old_path': 'drivers/net/ethernet/intel/ixgbe/ixgbe.h',
                            'new_path': 'drivers/net/ethernet/intel/ixgbe/ixgbe.h'},
                           {'old_path': 'include/linux/mm.h',
                            'new_path': 'include/linux/mm2.h'},
                           {'old_path': 'include/net/bonding.h',
                            'new_path': 'include/net/bond_xor.h'}]
               }

    def test_load_yaml_data(self):
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            result = webhook.subsystems.load_yaml_data('bad_map_file.yml')
            self.assertEqual(result, None)
            self.assertIn("No such file or directory: 'bad_map_file.yml'", logs.output[-1])

        with mock.patch('builtins.open', mock.mock_open(read_data=self.FILE_CONTENTS)):
            result = webhook.subsystems.load_yaml_data('map_file.yml')
            self.assertEqual(result, self.MAPPINGS)

    def test_user_wants_notification(self):
        user_data = {'all': ['net/'],
                     '8.y': ['include/net/bonding.h'],
                     '8.2': ['include/net/*'],
                     '8.1': ['*/net/*']}

        path_list = ['net/core/dev.c']
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['networking/ipv8.c']
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bond_3ad.h']
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding.h']
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding/bonding.h']
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding/bonding.h']
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

    @mock.patch('os.listdir')
    @mock.patch('webhook.subsystems.load_yaml_data')
    @mock.patch('webhook.subsystems.user_wants_notification')
    def test_do_usermapping(self, mock_user_wants, mock_loader, mock_listdir):
        # Cannot get file listing.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_listdir.side_effect = PermissionError
            result = webhook.subsystems.do_usermapping('8.y', [], '/repo')
            self.assertEqual(result, [])
            self.assertIn('Problem listing path', logs.output[-1])
            mock_user_wants.assert_not_called()

        # Error loading user data.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_listdir.side_effect = None
            mock_listdir.return_value = ['user1']
            mock_loader.side_effect = [None]
            result = webhook.subsystems.do_usermapping('8.y', [], '/repo')
            self.assertEqual(result, [])
            self.assertIn("Error loading user data from path '/repo/users/user1'.",
                          logs.output[-1])
            mock_user_wants.assert_not_called()

        mock_listdir.return_value = ['user1', 'user2', 'user3']
        mock_loader.side_effect = None
        path_list = ['include/net/bonding.h', 'net/core/dev.c']

        user1_data = {'all': ['include/net/bonding.h']}
        user2_data = {'all': ['drivers/net/bonding/']}
        user3_data = {'all': ['include/net/bonding.h']}
        mock_loader.side_effect = [user1_data, user2_data, user3_data]
        mock_user_wants.side_effect = [True, False, True]
        result = webhook.subsystems.do_usermapping('8.y', path_list, '/repo')
        call_list = [mock.call(user1_data, path_list, '8.y'),
                     mock.call(user2_data, path_list, '8.y'),
                     mock.call(user3_data, path_list, '8.y')]

        self.assertEqual(mock_user_wants.call_count, 3)
        mock_user_wants.assert_has_calls(call_list, any_order=True)
        self.assertEqual(sorted(result), ['user1', 'user3'])

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_post_notifications(self):
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        mock_mr.participants.return_value = [{'username': 'user1'},
                                             {'username': 'user2'},
                                             {'username': 'user3'}]

        # No one new to notify.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            user_list = ['user1', 'user3']
            webhook.subsystems.post_notifications(mock_mr, user_list, 'https://gitlab.com/project1')
            self.assertIn('No one new to notify.', logs.output[-1])
            mock_mr.participants.assert_called()
            mock_mr.notes.create.assert_not_called()

        # Create a note.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            user_list = ['user1', 'user2', 'user4', 'user5']
            template = webhook.subsystems.NOTIFICATION_TEMPLATE
            note_text = template.format(header=webhook.subsystems.NOTIFICATION_HEADER,
                                        users='@user4 @user5',
                                        project='https://gitlab.com/project1')
            webhook.subsystems.post_notifications(mock_mr, user_list, 'https://gitlab.com/project1')
            self.assertIn('Posting notification on MR 2:', logs.output[-1])
            mock_mr.participants.assert_called()
            mock_mr.notes.create.assert_called_with({'body': note_text})

    def test_get_subsystems(self):
        result = webhook.subsystems.get_subsystems('net/core/dev.c', self.MAPPINGS['subsystems'])
        self.assertEqual(result, ['net'])
        result = webhook.subsystems.get_subsystems('tools/testing/selftests/bpf/test_xdp_veth.sh',
                                                   self.MAPPINGS['subsystems'])
        self.assertEqual(sorted(result), ['xdp'])
        result = webhook.subsystems.get_subsystems('weirdfile.bat', self.MAPPINGS['subsystems'])
        self.assertEqual(result, [])

    def test_glob_match(self):
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/*')
        self.assertEqual(result, False)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/bin/*')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/bin/f[ab]*')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/bin/f[xy]*')
        self.assertEqual(result, False)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/bin/*al*')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/bin/false')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/*/false')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/???/false')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/????/false')
        self.assertEqual(result, False)
        result = webhook.subsystems.glob_match('usr/bin/false', '*')
        self.assertEqual(result, False)
        result = webhook.subsystems.glob_match('usr/bin/false', '*/*')
        self.assertEqual(result, False)
        result = webhook.subsystems.glob_match('usr/bin/false', '*/*/*')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/bin/')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/sbin/')
        self.assertEqual(result, False)
        result = webhook.subsystems.glob_match('usr/bin/false', '*/')
        self.assertEqual(result, True)
        result = webhook.subsystems.glob_match('usr/bin/false', 'usr/bin')
        self.assertEqual(result, False)

    def test_get_blocking_labels(self):
        # no existing label
        result = webhook.subsystems.get_blocking_labels(['net'], self.MAPPINGS['subsystems'])
        self.assertEqual(result, ['lnst::NeedsTesting'])

    def test_make_labels(self):
        topics = ['driver ixgbe', 'include', 'net', 'lnst::NeedsTesting']
        expected_list = [{'name': f'{webhook.subsystems.DRIVER_LABEL_PREFIX}:ixgbe',
                          'color': webhook.subsystems.DRIVER_LABEL_COLOR,
                          'description': webhook.subsystems.DRIVER_LABEL_DESC % 'ixgbe'},
                         {'name': f'{webhook.subsystems.SUBSYS_LABEL_PREFIX}:include',
                          'color': webhook.subsystems.SUBSYS_LABEL_COLOR,
                          'description': webhook.subsystems.SUBSYS_LABEL_DESC % 'include'},
                         {'name': f'{webhook.subsystems.SUBSYS_LABEL_PREFIX}:net',
                          'color': webhook.subsystems.SUBSYS_LABEL_COLOR,
                          'description': webhook.subsystems.SUBSYS_LABEL_DESC % 'net'},
                         {'name': 'lnst::NeedsTesting',
                          'color': webhook.common.NEEDS_TESTING_LABEL_COLOR,
                          'description': webhook.subsystems.NEEDS_TESTING_LABEL_DESC % 'lnst'},
                         ]

        result = webhook.subsystems.make_labels(topics)
        self.assertEqual(len(result), 4)
        self.assertEqual(result, expected_list)

    def test_get_mr_pathlist(self):
        mock_mr = mock.Mock()
        mock_mr.changes.return_value = {}
        result = webhook.subsystems.get_mr_pathlist(mock_mr)
        self.assertEqual(result, [])
        mock_mr.changes.return_value = self.CHANGES
        result = webhook.subsystems.get_mr_pathlist(mock_mr)
        self.assertEqual(sorted(result), ['drivers/net/ethernet/intel/ixgbe/ixgbe.h',
                                          'include/linux/mm.h',
                                          'include/linux/mm2.h',
                                          'include/net/bond_xor.h',
                                          'include/net/bonding.h'])

    def test_do_mapping(self):
        mock_mr = mock.Mock()
        mock_mr.commits.return_value = [1]

        # Two net paths match 'net', trigger 'lnst' readyForMergeDeps.
        with self.assertLogs('cki.webhook.subsystems', level='DEBUG') as logs:
            path_list = ['net/core/dev.c', 'include/net/ip.h']
            result = webhook.subsystems.do_mapping(path_list, self.MAPPINGS['subsystems'])
            self.assertEqual(len(result), 2)
            result_names = sorted([label['name'] for label in result])
            self.assertEqual(result_names[0], 'Subsystem:net')
            self.assertEqual(result_names[1], 'lnst::NeedsTesting')
            self.assertIn("Path 'net/core/dev.c' matched topics: ['net']",
                          ''.join(logs.output))
            self.assertIn("Path 'include/net/ip.h' matched topics: ['net']",
                          ''.join(logs.output))

        # xdp paths match regex.
        with self.assertLogs('cki.webhook.subsystems', level='DEBUG') as logs:
            path_list = ['include/net/ip.h', 'include/net/xdp_priv.h']
            result = webhook.subsystems.do_mapping(path_list, self.MAPPINGS['subsystems'])
            print('\n'.join(logs.output))
            self.assertEqual(len(result), 3)
            result_names = sorted([label['name'] for label in result])
            self.assertEqual(result_names[0], 'Subsystem:net')
            self.assertEqual(result_names[1], 'Subsystem:xdp')
            self.assertEqual(result_names[2], 'lnst::NeedsTesting')
            self.assertIn("Path 'include/net/ip.h' matched topics: ['net']",
                          ''.join(logs.output))
            self.assertIn("Path 'include/net/xdp_priv.h' matched topics: ['net', 'xdp']",
                          ''.join(logs.output))

        # No matches.
        with self.assertLogs('cki.webhook.subsystems', level='DEBUG') as logs:
            path_list = ['weirdfile.bat']
            result = webhook.subsystems.do_mapping(path_list, self.MAPPINGS['subsystems'])
            self.assertEqual(result, None)
            self.assertIn("No matching subsystem topics found for path 'weirdfile.bat'",
                          ''.join(logs.output))
            self.assertIn("No matching subsystem topics found for MR file list: ['weirdfile.bat']",
                          ''.join(logs.output))

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.subsystems.post_notifications')
    @mock.patch('webhook.subsystems.do_usermapping')
    @mock.patch('webhook.subsystems.do_mapping')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.subsystems.get_mr_pathlist')
    @mock.patch('webhook.subsystems.load_yaml_data')
    def test_process_mr(self, mock_loader, mock_pathlist, mock_add_label, mock_mapping,
                        mock_usermapping, mock_poster, mock_gl):
        project_url = 'https://gitlab.com/project123'
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        mock_mr.commits.return_value = [1]
        mock_mr.target_branch = 'main'
        mock_project = mock.Mock()
        mock_project.namespace = {'name': '8.y'}
        mock_gl.projects.get.return_value = mock_project
        mock_project.mergerequests.get.return_value = mock_mr
        mock_mr.labels = []
        mock_pathlist.return_value = []
        mock_usermapping.return_value = []

        msg = mock.Mock()
        msg.payload = {}
        msg.payload['project'] = {'id': 1}
        msg.payload['changes'] = {'labels': {'previous': [],
                                             'current': [{'title': 'Subsystem:'}]
                                             }
                                  }
        msg.payload['object_attributes'] = {'iid': 2, 'action': 'open'}

        # No path changes.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            webhook.subsystems.process_mr(mock_gl, msg, 'map_file.yml', '/data/repo', project_url)
            self.assertIn('MR 2 does not report any changed files, exiting.', logs.output[-1])
            mock_mapping.assert_not_called()
            mock_usermapping.assert_not_called()
            mock_poster.assert_not_called()
            mock_add_label.assert_not_called()

        # No yaml data.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_pathlist.return_value = ['net/core/dev.c']
            mock_loader.return_value = None
            webhook.subsystems.process_mr(mock_gl, msg, 'bad_map_file.yml', '/data/repo',
                                          project_url)
            err_str = "Expected mapping data not loaded from 'bad_map_file.yml', skipping labeling."
            self.assertIn(err_str, logs.output[-1])
            mock_mapping.assert_not_called()
            mock_add_label.assert_not_called()
            mock_usermapping.assert_called()

        # No label to add.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            mock_loader.return_value = self.MAPPINGS
            mock_pathlist.return_value = ['weirdfile.bat']
            mock_mapping.return_value = []
            webhook.subsystems.process_mr(mock_gl, msg, 'map_file.yml', '/data/repo', project_url)
            self.assertIn('No labels to add.', ' '.join(logs.output))
            mock_mapping.assert_called_with(['weirdfile.bat'], self.MAPPINGS['subsystems'])
            mock_add_label.assert_not_called()
            mock_usermapping.assert_called()

        # Label, no users to map.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            mock_pathlist.return_value = ['net/core/dev.c']
            name = f'{webhook.subsystems.SUBSYS_LABEL_PREFIX}:net'
            color = webhook.subsystems.SUBSYS_LABEL_COLOR
            desc = webhook.subsystems.SUBSYS_LABEL_DESC % 'net'
            label = {'name': name, 'color': color, 'description': desc}
            mock_mapping.return_value = [label]
            mock_usermapping.return_value = []
            webhook.subsystems.process_mr(mock_gl, msg, 'map_file.yml', '/data/repo', project_url)
            mock_add_label.assert_called_with(mock_project, 2, [label])
            mock_usermapping.assert_called_with('8.y', mock_pathlist.return_value, '/data/repo')
            mock_poster.assert_not_called()
            self.assertIn('No new users to notify @ for MR 2.', logs.output[-1])

        # Labels & user mapped.
        mock_usermapping.return_value = ['user1']
        webhook.subsystems.process_mr(mock_gl, msg, 'map_file.yml', '/data/repo', project_url)
        mock_add_label.assert_called_with(mock_project, 2, [label])
        mock_usermapping.assert_called_with('8.y', mock_pathlist.return_value, '/data/repo')
        mock_poster.assert_called_with(mock_mr, ['user1'], project_url)
