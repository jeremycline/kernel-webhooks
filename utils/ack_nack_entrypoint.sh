#!/usr/bin/env bash

set -e

# The ACK/NACK webhook needs to write out some temporary files into the skeleton
# linux tree. OpenShift sets up the root filesystem inside the container as
# readonly. We can write into /data though. When the container starts, copy
# the cached copy that was 

cp -dpR /usr/src/linux /data/linux
export LINUX_SRC=/data/linux
export RHKERNEL_SRC=/data/rhkernel

# Let's download the latest copy of the get_maintainers.pl and RHMAINTAINERS
# file on container startup. Ignore any errors since there can be transient
# network errors.
/code/utils/download_maintainers.sh || true

exec cki_entrypoint.sh
